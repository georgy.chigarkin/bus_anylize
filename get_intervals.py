import dataclasses
import os
from typing import List, Dict, Tuple

from datetime import datetime, timedelta


@dataclasses.dataclass
class Point:
    lon: float
    lat: float
    time: str
    direction: int
    gos_num: str
    type: str
    number: str

    @property
    def timestamp(self):
        return datetime.strptime(self.time, "%d.%m.%Y %H:%M:%S").timestamp()


def get_points_distance(lon, lat, point: Point):
    return abs(((lon - point.lon) ** 2 + (lat - point.lat) ** 2) ** (1 / 2))


def get_nearest_point(lon, lat, points: List[Point]):
    if len(points) == 0:
        return
    nearest_point = points[0]
    best_distance = get_points_distance(lon, lat, nearest_point)
    for point in points:
        distance = get_points_distance(lon, lat, point)
        if distance < best_distance:
            best_distance = distance
            nearest_point = point

    return nearest_point


def separate_points(points: List[Point]) -> List[List[Point]]:
    if len(points) == 0:
        return list(list())
    if len(points) == 1:
        return [points, ]
    result = []
    current = points[0]
    current_list = list()
    current_list.append(current)
    for i in range(1, len(points)):
        if abs(current.timestamp - points[i].timestamp) < 30 * 60:
            current_list.append(points[i])
        else:
            current = points[i]
            result.append(current_list)
            current_list = list()
    return result


def load_csv(abs_path):
    with open(abs_path, encoding="utf-8") as f:
        points_lines = f.readlines()[1:]
        points = [Point(float(line[1]), float(line[2]), line[5], int(line[3]), line[6], line[9], line[8]) for line in
                  [t.split(";") for t in points_lines]]

    return points


def find_closest_index(points, timestamp):
    closest = points[0]
    index = 0
    for i, point in enumerate(points):
        if abs(point.timestamp - timestamp) < abs(closest.timestamp - timestamp):
            closest = points
            index = i
    return index


def get_streets() -> List[dict]:
    streets = []
    with open("streets.csv", "r", encoding="utf-8") as f:
        lines = f.readlines()
        for line in lines[1:]:
            line = line.replace('"', "").replace("\n", "")
            streets.append({
                "name": line.split(";")[0],
                "start_lon": float(line.split(";")[1]),
                "start_lat": float(line.split(";")[2]),
                "end_lon": float(line.split(";")[3]),
                "end_lat": float(line.split(";")[4]),
                "trolley": [str(p) for p in line.split(";")[5].split(",")],
                "busses": [str(p) for p in line.split(";")[6].split(",")],
            })
    return streets


def filter_points(points, trolleys, busses) -> Dict[str, List]:
    result = dict()
    for point in points:
        if point.type == "М":
            continue
        if point.type == "Т" and str(point.number) not in trolleys:
            continue
        if point.type == "А" and str(point.number) not in busses:
            continue
        if point.gos_num not in result.keys():
            result[point.gos_num] = list()
        result[point.gos_num].append(point)
    return result


def find_starts_and_ends(vehicle_points, start_lon, start_lat, end_lon, end_lat) -> Tuple:
    starts = []
    ends = []
    for i, point in enumerate(vehicle_points):
        if get_points_distance(start_lon, start_lat, point) < 10000:
            if i + 15 >= len(vehicle_points):
                continue
            if get_points_distance(end_lon, end_lat, vehicle_points[i + 15]) < get_points_distance(end_lon, end_lat,
                                                                                                   point):
                starts.append(point)
        if get_points_distance(end_lon, end_lat, point) < 10000:
            if i - 15 < 0:
                continue
            if get_points_distance(start_lon, start_lat, vehicle_points[i - 15]) < get_points_distance(start_lon,
                                                                                                       start_lat,
                                                                                                       point):
                ends.append(point)

    separated = separate_points(starts)
    starts = [get_nearest_point(start_lon, start_lat, i) for i in separated if
              get_nearest_point(start_lon, start_lat, i) is not None]

    separated = separate_points(ends)
    ends = [get_nearest_point(end_lon, end_lat, i) for i in separated if
            get_nearest_point(end_lon, end_lat, i) is not None]

    return (starts, ends)


if __name__ == "__main__":
    files = [os.path.join("./csv", file) for file in ["2022.02.21.csv",
                                                      "2022.02.22.csv", "2021.12.23.csv", "2021.12.24.csv",
                                                      "2022.02.24.csv", "2021.12.25.csv"]]
    points = list()
    for file in files:
        points += load_csv(file)

    streets = get_streets()

    for street_index, street in enumerate(streets):
        trolley = street["trolley"]
        bus = street["busses"]

        START_LON, START_LAT = street["start_lon"], street["start_lat"]
        END_LON, END_LAT = street["end_lon"], street["end_lat"]

        filtered_points = filter_points(points, trolley, bus)

        intervals = dict()
        for number in filtered_points.keys():
            vehicle_points = filtered_points[number]
            starts, ends = find_starts_and_ends(vehicle_points, START_LON, START_LAT, END_LON, END_LAT)

            if len(starts) == 0:
                continue

            if len(ends) == 0:
                continue

            intervals[number] = []
            for start in starts:
                for end in ends:
                    if abs(start.timestamp - end.timestamp) < 30 * 60:
                        intervals[number].append((start, end, vehicle_points[0].type, vehicle_points[0].number))

        with open(f"intervals{street_index}.csv", "w", encoding="utf-8") as f:
            for number in intervals.keys():
                for interval in intervals[number]:
                    dt = datetime.strptime(interval[0].time, "%d.%m.%Y %H:%M:%S")
                    dt = dt + timedelta(hours=3)
                    duration = round((interval[1].timestamp - interval[0].timestamp) / 60, 2)
                    if duration > 0:
                        f.write(f"{dt};{duration};{interval[2]};{interval[3]}\n")
