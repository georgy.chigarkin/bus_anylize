import os
import json
from datetime import datetime, timedelta
import shutil

dir_path = os.path.join(".")
csv_dir = os.path.join(dir_path, "csv")
json_path = os.path.join(dir_path, "json")


def move_files():
    files = [file for file in os.listdir(json_path) if file.endswith(".json")]
    for name in files:
        timestamp = float(name.split(".")[0])
        dt = datetime.fromtimestamp(timestamp) - timedelta(hours=3)
        formatted_date = dt.strftime("%Y.%m.%d")
        move_dir_path = os.path.join(json_path, formatted_date)
        if not os.path.exists(move_dir_path):
            os.mkdir(move_dir_path)
        shutil.move(os.path.join(json_path, name), os.path.join(move_dir_path, name))


def to_csv(dir_name):
    files_dir = os.path.join(json_path, dir_name)
    files = [file for file in os.listdir(files_dir) if file.endswith(".json")]
    with open(os.path.join(csv_dir, f"{dir_name}.csv"), "w", encoding="utf-8") as f:
        f.write("id;lon;lat;dir;speed;lasttime;gos_num;rid;rnum;rtype;low_floor;wifi;anim_key;big_jump\n")
        for name in files:
            with open(os.path.join(files_dir, name), encoding="utf-8") as file:
                data = json.load(file)
                for point in data["anims"]:
                    f.write(f"{point['id']};{point['lon']};{point['lat']};{point['dir']};{point['speed']};{point['lasttime']};{point['gos_num']};{point['rid']};{point['rnum']};{point['rtype']};{point['low_floor']};{point['wifi']};{point['anim_key']};{point['big_jump']}\n")


move_files()
to_csv("2022.02.22")
