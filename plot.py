from datetime import datetime
from typing import List

import matplotlib.pyplot as plt
import pandas as pd

month = ""


def get_streets() -> List[dict]:
    streets = []
    with open("streets.csv", "r", encoding="utf-8") as f:
        lines = f.readlines()
        for line in lines[1:]:
            line = line.replace('"', "").replace("\n", "")
            streets.append({
                "name": line.split(";")[0],
                "start_lon": float(line.split(";")[1]),
                "start_lat": float(line.split(";")[2]),
                "end_lon": float(line.split(";")[3]),
                "end_lat": float(line.split(";")[4]),
                "trolley": [p for p in line.split(";")[5].split(",")],
                "busses": [p for p in line.split(";")[6].split(",")],
            })
    return streets


streets = get_streets()

filenames = [f"intervals{i}.csv" for i in range(len(streets))]

for file_index, file in enumerate(filenames):

    df = pd.read_csv(file, names=["start", "duration", "type", "number"], sep=";", encoding="utf-8")

    df["start"] = pd.to_datetime(df["start"], format="%Y-%m-%d %H:%M:%S")
    df.start = df.start.apply(lambda x: datetime(2022, 2, 1, x.hour, x.minute))
    df = df.set_index(pd.DatetimeIndex(df["start"]))
    df.sort_index(inplace=True)

    df_slice = df

    quantile_0015 = df_slice["duration"].quantile(0.015)
    quantile_0985 = df_slice["duration"].quantile(0.985)

    df_slice = df_slice[df_slice["duration"] > quantile_0015]
    df_slice = df_slice[df_slice["duration"] < quantile_0985]

    df_t = df_slice[df_slice["type"] == "Т"]
    df_a = df_slice[df_slice["type"] == "А"]

    d = df_slice.resample('60Min', on='start', offset='30Min').duration.mean()
    d = d.resample('S').interpolate(method="quadratic")

    ax = plt.subplot(111)
    ax.set_ylim(df_slice.duration.min() - 1, df_slice.duration.max() + 1)
    col = df_slice.type.map({"Т": "b", "А": "r"})

    d.reset_index().plot(x="start", y="duration", kind='line', figsize=(20, 10), ax=ax)
    df_slice.resample('60Min', on='start', offset='30Min').duration.quantile(0.9).resample('S').interpolate(method="quadratic").reset_index().plot(x="start", y="duration", kind='line', figsize=(20, 10), ax=ax)
    df_slice.resample('60Min', on='start', offset='30Min').duration.quantile(0.1).resample('S').interpolate(method="quadratic").reset_index().plot(x="start", y="duration", kind='line', figsize=(20, 10), ax=ax)
    df_slice.plot(x="start", y="duration", kind='scatter', figsize=(20, 10), ax=ax, c=col, alpha=0.05)
    ax.legend(labels=["Среднее время в пути", "0.9 квантиль", "0.1 квантиль"])
    ax.set_xlabel("Время начала движения по участку")
    ax.set_ylabel("Продолжительность движения по участку (минут)")
    ax.title.set_text(streets[file_index]["name"])
    plt.savefig(f"./plots/{month}-{streets[file_index]['name']}.png")
    ax.cla()
